$(document).ready(function() {
    function loadAllPosts() {
        $.get('https://jsonplaceholder.typicode.com/posts', function(data) {
            let postsHtml = '';
            data.forEach(function(post) {
                postsHtml += `
                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">${post.title}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">Author: ${post.userId}</h6>
                            <p class="card-text">${post.body}</p>
                            <button class="btn btn-primary editPost" data-id="${post.id}">Edit</button>
                            <button class="btn btn-danger deletePost" data-id="${post.id}">Delete</button>
                        </div>
                    </div>
                `;
            });
            $('#content').html(postsHtml);
        });
    }

    loadAllPosts();

    $('#viewAllPosts').click(function(e) {
        e.preventDefault();
        loadAllPosts();
    });

    $('#createPost').click(function(e) {
        e.preventDefault();
        $('#content').load('create_post.html');
    });

    $(document).on('click', '.editPost', function() {
        let postId = $(this).data('id');
        $('#content').load('edit_post.html', function() {
            $.get(`https://jsonplaceholder.typicode.com/posts/${postId}`, function(data) {
                $('#editId').val(data.id);
                $('#editTitle').val(data.title);
                $('#editAuthor').val(data.userId);
                $('#editBody').val(data.body);
            });
        });
    });

    $(document).on('submit', '#createForm', function(e) {
        e.preventDefault();
        let formData = $(this).serialize();
        $.post('https://jsonplaceholder.typicode.com/posts', formData, function() {
            loadAllPosts();
        });
    });

    $(document).on('submit', '#editForm', function(e) {
        e.preventDefault();
        let postId = $('#editId').val();
        let formData = $(this).serialize();
        $.ajax({
            url: `https://jsonplaceholder.typicode.com/posts/${postId}`,
            type: 'PUT',
            data: formData,
            success: function() {
                loadAllPosts();
            }
        });
    });

    $(document).on('click', '.deletePost', function() {
        let postId = $(this).data('id');
        $.ajax({
            url: `https://jsonplaceholder.typicode.com/posts/${postId}`,
            type: 'DELETE',
            success: function() {
                loadAllPosts();
            }
        });
    });
});